Ejercicios Android 2016-2017
============================

* **videojuegos**: Ejercicio que muestra ejemplos sobre cómo comunicar Activities y trabajar con ListView
* **ejercicio1**: Ejercicio 1 de los apuntes
* **ListaPersonalizada**: Ejemplo de aplicación con ListView personalizada
* **PuntosWifi**: Aplicación que lista los puntos Wifi del ayuntamiento de Zaragoza para visualizarlos de diferente forma en un mapa Mapbox
* **HolaLibGDX**: Juego de naves como introducción a libGDX
