package com.sfaci.hola.util;

/**
 * Created by dam on 30/01/18.
 */
public class Constantes {

    public static String SONIDO_GOTA = "waterdrop.wav";
    public static String MUSICA_LLUVIA = "undertreeinrain.mp3";

    public static String TEXTURA_CUBO = "bucket";
}
