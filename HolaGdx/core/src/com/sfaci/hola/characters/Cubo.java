package com.sfaci.hola.characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by dam on 16/01/18.
 */
public class Cubo extends Personaje {

    public Cubo(TextureRegion imagen, float x, float y) {
        super(imagen, x, y);
    }
}
